/**
 * Hos5.net
 * Lokasyon tabanlı anonim sohbet servisi
 *
 * @author Alpcan AYDIN <alpcan@alpcanaydin.com>
 * @version 0.1
 */

 // Load config module
var config = require("../helpers/config");

// Load city model
require("../models/city");

// Load mongoose and create connection to hos5 database on given port
var mongoose = require("mongoose");
var db = mongoose.createConnection(config.config.mongodb.host, config.config.mongodb.database, config.config.mongodb.port);


// Get home page
exports.index = function(req, res){
	// Assign city model to a variable
	var City = db.model("City");

	// Get all cities
	var cities = City.find({}).sort('plate').exec(function(err, cities) {
		// Render the home template
		res.render("index", { cities: cities});
	});
};
