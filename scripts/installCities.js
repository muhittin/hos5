/**
 * Hos5.net
 * Lokasyon tabanlı anonim sohbet servisi
 *
 * @author Alpcan AYDIN <alpcan@alpcanaydin.com>
 * @version 0.1
 */

 // Load config module
var config = require("../helpers/config");

// Load file system module
var fs = require('fs');

// Load mongoose and create connection to hos5 database on given port
var mongoose = require("mongoose");
var db = mongoose.createConnection(config.config.mongodb.host, config.config.mongodb.database, config.config.mongodb.port);

// Load city model
require("../models/city");

console.log("Cities installation has been started.");

// Assign city model to a variable
var City = db.model("City");

// Truncate city collection and insert new cities
City.remove({}, function(err) {
	// Check if error occured
	if (!err) {
		console.log("City collection has been truncated, and new cities will be installed.");

		// Get city data
		var data = fs.readFileSync(__dirname + "/../data/cities", "utf-8");

		// Make it array
		var citiesData = data.toString().split("\n");

		// Parse each city
		citiesData.forEach(function(cityData) {
			// If EOF then stop hero!
			if (cityData.length == 0) return;

			// Splite plate and city title
			var cityData = cityData.split("-");

			// Assign them into variables
			var plate = parseInt(cityData[0]);
			var title = cityData[1];

			// Create city object
			var addProcessing = new City({
				plate: plate,
				title: title
			});

			// Save it bro, we need them!
			addProcessing.save(function(err) {
				if (err) {
					console.log("%s has not been added.", title);
				} else {
					console.log("%s has been added with plate %d", title, plate);
				}
			});
		});
	} else {
		console.log("An error occured while truncating the city collection.");
	}
});

