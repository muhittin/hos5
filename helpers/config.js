/**
 * Hos5.net
 * Lokasyon tabanlı anonim sohbet servisi
 *
 * @author Alpcan AYDIN <alpcan@alpcanaydin.com>
 * @version 0.1
 */

// Load file system module
var fs = require('fs');

// Assign config parameters to variable synchronously
var config = JSON.parse(fs.readFileSync(__dirname + "/../config.json", "utf8"));

// Export the config variable
module.exports.config = config;
