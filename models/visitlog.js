/**
 * Hos5.net
 * Lokasyon tabanlı anonim sohbet servisi
 *
 * @author Alpcan AYDIN <alpcan@alpcanaydin.com>
 * @version 0.1
 */

// Load mongoose module
var mongoose = require("mongoose");

// Create visitlog schema for model
var visitLogSchema = new mongoose.Schema({
	// Set id of client
	clientID: String,

	// Client's IP
	ip: String,

	// When this client visited?
	visitedAt: { type: Date, default: Date.now }
});

// Export visitlog model
module.exports.visitLogModel = mongoose.model('VisitLog', visitLogSchema);

