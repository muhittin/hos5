/**
 * Hos5.net
 * Lokasyon tabanlı anonim sohbet servisi
 *
 * @author Alpcan AYDIN <alpcan@alpcanaydin.com>
 * @version 0.1
 */

// Load mongoose module
var mongoose = require("mongoose");

// Create city schema for model
var citySchema = new mongoose.Schema({
	// Plate of city, It can be between 0 and 81
	// 82 reference to All country
	plate:  { type: Number, min: 0, max: 81 },

	// Title of city
	// Example: Düzce
	title: String,

	// Count of current visitors who are in that city right now.
	// It can be minimum 0
	count: { type: Number, default: 0, min: 0},

	// All count of all visits
	visits: { type: Number, default: 0, min: 0}
});

// Export city model
module.exports.cityModel = mongoose.model('City', citySchema);

